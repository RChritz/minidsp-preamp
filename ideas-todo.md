# Ideas/To-do

## VirtualHere USB over IP server
https://www.virtualhere.com/ ($49)


VirtualHere USB over IP server. Used for programming the MiniDSP without opening the enclosure. A Virtualhere client runs on some other machine, connects to the server when available.

~~Jun 2019 (RaspberryPi 3B+): the VirtualHere kernel panics when trying to play audio from a remote client. Could be, at least, used to program the Arduino?~~

Nov 2019: the latest version of VirtualHere, combined with a RaspberryPi 4 seems to be working. The DDRC-24 plugin connects and is able to control the device, Dirac Live 2 also connects and is able to play test tones. Further testing needed.

The plan, in case it works as intended, is to create a simple menu system:

* press/hold a button (maybe on the rotary encoder? We lose mute, if so)
  * open a menu with two selections:
    * 01 start Dirac calibration
      * switch the DDRC-24 input to USB
      * switch output to speakers (Profile 1)
      * ensure that the DDRC-24 is disconnected in Python
      * ensure that the Squeezelite client is stopped
      * start the VirtualHere server
      * (possible problem: Arduino<->Raspberry Pi connection broken, what then?)
    * 02 stop Dirac calibration
      * gracefully stop the VirtualHere server
      * ensure that there's a connection to the DDRC-24 in Python
      * start the Squeezelite client
      * restore the state (inputs/outputs/dirac/volume/...) - not needed?
* simpler idea:
  * hold the Dirac Live button for x cycles (or ms, whatever)
  * disable all buttons/ir other than the Dirac one
  * proceed as menu 01 above
  * when finished hold the Dirac button for x cycles
  * proceed as menu 02 above

## REST API
REST API in Python and "companion" control app for Android and/or iOS?
  * same functionality as ir remote,
  * edit input/output names & input volume settings
  * check for updates?

## Automatic updates
Arduino & Python code auto update?
  * git clone the .ino and Python code at boot?
  * check a specific path for new files (which you upload via SSH)?
  * ping a web service & download if newer?

## User interface/menus
* start/stop VirtualHere server for Dirac Live calibration
* turn power amplifiers on or off (don't need speakers if listening through headphones)
* rename inputs/outputs, change the target volume level
* remapping of buttons/rotary/ir remote when in menu
  * rotary left/right = menu up/down
  * rotary button = select/enter
  * minidsp ir 1/4 = up down
  * minidsp ir dirac = select/enter
* tentative structure:
  * turn on power amplifiers
  * turn off power amplifiers
  * start Dirac calibration
  * stop Dirac calibration
  * display IP/hostname
  * exit

## Store configuration on the RaspberryPi
Store the configuration (IR remote codes, last active input and output, input/output name, input target volume) on the Raspberry Pi and read it on boot.

## Input dependant volume settings (done)
* Set the target input volume (target volume > 0: target volume not set)
* Calculate the difference between current and target volumes
* Adjust volume accordingly

## Other
* Make the Raspberry Pi SDCard read only.
  * Store syslog on a ramdisk?
  * How to save configuration?