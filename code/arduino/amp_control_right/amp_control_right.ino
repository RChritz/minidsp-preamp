// poweramp right

#include <RBD_Timer.h>
#include <RBD_Button.h>

#define led 11
RBD::Button btn_pwr(8);
#define relay 10
#define ldr A6

int state_pwr = 0;
String input_string = "";
bool string_complete = false;

int ldr_value = 0;
int led_value = 0;

void setup() {
  pinMode(led, OUTPUT);
  pinMode(relay, OUTPUT);

  digitalWrite(led, LOW);
  digitalWrite(relay, LOW);

  state_pwr = 0;

  Serial.begin(2400);
  input_string.reserve(10);
}

void loop() {
  if (btn_pwr.onPressed()) {
    switch(state_pwr) {
      case 0: {
        power_on();
        break;
      }
      case 1: {
        power_off();
        break;
      }
    }
  }

  if (state_pwr == 1) {
    ldr_value = (analogRead(ldr))/4;
    led_value = 255 - (ldr_value > 254 ? 254 : ldr_value);
    analogWrite(led, led_value);
  }
}

void power_on() {
  digitalWrite(led, HIGH);
  digitalWrite(relay, HIGH);
  Serial.println(1);
  state_pwr = 1;
}

void power_off() {
  digitalWrite(led, LOW);
  digitalWrite(relay, LOW);
  Serial.println(0);
  state_pwr = 0;
}

void serialEvent() {
  while(Serial.available()) {
    char in_char = (char)Serial.read();
    input_string += in_char;
    if (in_char == '\n') {
      string_complete = true;
    }
  }
  
  if (string_complete) {
    int cmd = input_string.toInt();
    switch (cmd) {
      case 0: {
        power_off();
        break;
      }
      case 1: {
        for (int i = 0; i == 2; i++) {
          digitalWrite(led, HIGH);
          delay(200);
          digitalWrite(led, LOW);
          delay(200);
        }
        power_on();
        break;
      }
    }
    input_string = "";
    string_complete = false;
  }
}

void serialEventRun(void) {
  if (Serial.available()) serialEvent();
}
