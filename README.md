# MiniDSP preamp

Complete audio preamplifier based on a <a href="https://www.minidsp.com/products/dirac-series/ddrc-24">MiniDSP DDRC-24</a>, controlled by an Arduino Mega 2560 via a Raspberry Pi.

The main goal was adding a user friendly(er) interface to the device, as it, originally, doesn't have a status display; it can be managed via an IR remote, but the status (volume level, selected source, selected profile, ...) can only be viewed via their software on a (Windows/Mac) computer. An added feature are (de)multiplexers for inputs and outputs; the MiniDSP DDRC-24 only has 1 TOSLINK and 1 analog stereo input. Multiplexing the 4 outputs allows us to switch between more than 2 (stereo) outputs. In my case I toggle between two stereo amplifiers and a headphone amplifier.

The user facing portion is handled by an Arduino, which sends commands over serial to a Raspberry Pi, which, in turn, sends raw HID commands to the MiniDSP (and receives/parses the responses from the device, then sends them back to the Arduino to be displayed on an OLED display).

Using the Raspberry Pi as a backend, of sorts, also allows for having an audio source play audio through USB (<a href="https://en.wikipedia.org/wiki/Squeezelite">Squeezelite</a>, in this case) and having a <a href="https://www.virtualhere.com/">VirtualHere</a> server running when needed. With VirtualHere we're able to calibrate the device via a network connection so there's no need to connect it directly to a computer. Works fine over 5GHz WiFi.

The MiniDSP DDRC-24/2x4HD driver is the work of Mark Kubiak @ https://github.com/markubiak/python3-minidsp. Only minor modifications were made by me.

MiniDSP raw USB command codes were obtained with a USB sniffer, additional commands were provided by the MiniDSP Devteam @ https://www.minidsp.com/forum/suggestion-box/14442-volume-display#38356.

## Features

* Volume and status display for the MiniDSP DDRC-24
* 4 TOSLINK inputs
* 4 stereo analog inputs
* 2x2 stereo analog outputs (multiplexed from the 4 MiniDSP outputs)
* Independent volume settings for inputs
* Squeezelite streamer on USB
* Remote control with an Apple remote or (preferrably) with the MiniDSP remote
* Rotary control for volume and mute
* A bunch of buttons (standby, switch inputs and outputs, toggle Dirac Live)
* 3 Serial ports on the back of the chassis (used for trigger control of 3 power amplifiers)
* turn connected power amplifiers on/off via remote/button over serial
* VirtualHere USB over IP server; connect to the DDRC-24 with Dirac Live and the DDRC-24 plugin over the network

## Included in this repository

* Arduino code
* Raspberry Pi/Python code and scripts
* PCBs for the input/output switches, front panel and Arduino
* Front/back panel designs
* PCBs and code for power amplifier trigger control circuit

## Block diagram

![block diagram](./img/block_diagram_2019-10-28.png "block diagram")

## Photos

### Build progress
<a href="https://imgur.com/a/kjtOxmh">Build progress gallery on imgur.</a>

### Finished product

Front detail:
![finished product front detail](./img/front_detail.jpg "finished product front detail")
Front:
![front](./img/front.jpg "front")
Back:
![back](./img/back.jpg "back")

## Renders

![final version front](./img/renders/final_render_front.jpg "final version front")

![final version back](./img/renders/final_render_back.jpg "final version back")

![final version front detail](./img/renders/final_render_front_detail.jpg "final version front detail")

![final version components only](./img/renders/final_render_components_only_relay.jpg "final version components only")

![final version components back](./img/renders/final_render_components_back_relay.jpg "final version components back")

### Arduino pin assignments

| pin | function |
| --- | ---- |
| A0  | photoresistor |
| D4  | LED |
| D5 | Relay channel 2 |
| D6 | Relay channel 1 |
| D7 | 3V3 trigger input for Logitech Squeezebox Classic |
| D14 | Serial 3 TX |
| D15 | Serial 3 RX |
| D16 | Serial 2 TX |
| D17 | Serial 2 RX |
| D18 | Serial 1 TX |
| D19 | Serial 1 RX |
| D20 | Digital input CD4052B A |
| D21 | Digital input CD4052B B |
| D22 | Toggle DSP button |
| D23 | Input 1 relay control |
| D24 | Cycle outputs button |
| D25 | Input 2 relay control |
| D26 | Cycle inputs button |
| D27 | Input 3 relay control |
| D28 | Power/standby button |
| D29 | Input 4 relay control |
| D30 | IR receiver |
| D31 | Output 2-1 relay control |
| D32 | Rotary mute button (MS) |
| D33 | Output 2-2 relay control |
| D34 | Rotary A (DT) |
| D35 | Output 1-1 relay control |
| D36 | Rotary B (CLK) |
| D37 | Output 1-2 relay control |
| D39 | Connected to Raspberry Pi GPIO 15 (UART TX) |
| D47 | OLED reset |
| D49 | OLED DC |
| D51 | OLED SDA/SPI MOSI |
| D52 | OLED SCL/SPI SCK |
| D53 | OLED CS/SPI SS |

## MiniDSP USB command codes

See <a href="./minidsp_usb_codes.md">minidsp_usb_codes.md</a>.

## Notes
### Installing Python and relevant packages

Tested with Raspbian Stretch Lite. ~~May work with newer versions.~~ Also works on the Raspberry Pi 4 and Raspbian Buster Lite.

Required software:
* Python3
* Python3-venv
* Python3-pip
* Pyhon3-hid
* Cython3

Python packages:
* hidapi
* pyserial

Install the software with:
~~~bash
sudo apt-get install python3 python3-hid cython3 python3-venv python3-pip
~~~

Download the Python code, store it somewhere (say ~/projects/python/minidsp), then create a new virtualenv:
~~~bash
cd ~/projects/python/minidsp
python3 -m venv ./minidsp-venv
~~~
Activate the virtualenv
~~~bash
source ./minidsp-venv/bin/activate
~~~
Install the packages from requirements.txt
~~~bash
pip install -r ./requirements.txt
~~~

### Allowing access to USB

Create /etc/udev/rules.d/99-minidsp.rules:
~~~
SUBSYSTEM=="usb", ATTR{idVendor}=="2752", ATTR{idProduct}=="0044", MODE="0660", GROUP="plugdev", TAG+="uaccess", TAG+="udev-acl"
KERNEL=="hidraw*", ATTRS{idVendor}=="2752", ATTRS{idProduct}=="0044", MODE="0660", GROUP="plugdev", TAG+="uaccess", TAG+="udev-acl"
~~~

### Allowing shutdown from Python

Create /etc/sudoers.d/pisudoers (replace pi with your username, if you've changed the default Raspbian user):
~~~
pi ALL=/sbin/shutdown
pi ALL=NOPASSWD: /sbin/shutdown
~~~

### Start the Python script at boot
Copy the Python code to somewhere like /usr/local/bin (including the virtual environment) and create a Systemd service in /etc/systemd/system/minidsp.service:
~~~
[Unit]
Description=Minidsp control service
After=multi-user.target

[Service]
Type=idle

ExecStart=/usr/local/bin/minidsp/minidsp-venv/bin/python /usr/local/bin/minidsp/minidsp.py

[Install]
WantedBy=multi-user.target
~~~
Start the service and enable starting at system boot:
~~~bash
sudo systemctl start minidsp
sudo systemctl enable minidsp
~~~

### Start squeezelite at boot
Install Squeezelite and create a Systemd service in /etc/systemd/system/squeezelite.service. You have to specify which soundcard Squeezelite will use (in this case CARD=DDRC24,DEV=0) and, optionally, a name for the player. More information in Squeezelite's man pages: https://manpages.ubuntu.com/manpages/trusty/man1/squeezelite.1.html
~~~
[Unit]
Description=Squeezelite
After=network.target

[Service]
ExecStart=/usr/bin/squeezelite -o hw:CARD=DDRC24,DEV=0 -n raspberrypi

[Install]
WantedBy=multi-user.target
~~~
Start the service and enable starting at system boot:
~~~bash
sudo systemctl start squeezelite
sudo systemctl enable squeezelite
~~~

### VirtualHere
#### General outline for the calibration:
- hold the btn_dsp button for longer than a second
- the Arduino sends a command over serial to the Raspberry Pi
- the Pi receives the command and starts the VirtualHere service
- the VirtualHere service is set up so it won't run at the same time as the MiniDSP Python service and the SqueezeLite player and will stop them when it starts
- connect to the VirtualHere server from another computer
- open the DDRC-24 plugin, Dirac Live and calibrate
- disconnect the device in the VirtualHere Client
- when the VirtualHere server detects that the DDRC-24 is unbound it runs a script that stops the VirtualHere service which, in turn, starts the MiniDSP Python service and the SqueezeLite player
- the MiniDSP Python service reinitializes the connection from the Arduino to the DDRC-24

Download the VirtualHere server from https://www.virtualhere.com/usb_server_software (Linux server for ARM). Save it somewhere like /usr/local/bin/virtualhere.

Create a service as per the instructions on https://www.virtualhere.com/oem_faq, except don't set it to autostart. The "Conflicts=" parameter is there so that when the VirtualHere server starts it'll stop the MiniDSP and Squeezelite services.
~~~
[Unit]
Description=VirtualHere
Requires=networking.service
After=networking.service
Conflicts=minidsp.service squeezelite.service

[Service]
ExecStartPre=/bin/sh -c 'logger VirtualHere settling...;sleep 1s;logger VirtualHere settled.'
ExecStart=/usr/local/bin/virtualhere/vhusbdarm
ExecStopPost=/usr/local/bin/virtualhere/scripts/virtualhere_shutdown.sh
Type=idle

[Install]
WantedBy=multi-user.target
~~~
Create a script that'll run when a client disconnects/unbinds the MiniDSP (see https://www.virtualhere.com/quirks for details). Save it somewhere like /usr/local/bin/virtualhere/scripts/minidsp_onunbind.sh and make it executable
~~~bash
#!/bin/sh
logger "Unbind DDRC24 $1 $2 $3 $4"
sudo systemctl stop virtualhere
~~~
Create a script that'll run when the VirtualHere systemd service is stopped.
/usr/local/bin/virtualhere/scripts/virtualhere_shutdown.sh:
~~~bash
#!/bin/sh
logger "Starting MiniDSP service"
sudo systemctl start minidsp
logger "Starting SqueezeLite"
sudo systemctl start squeezelite
~~~
Edit /usr/local/bin/virtualhere/config.ini and add an onUnbind parameter for the DDRC-24 (vendor id: 2752, device id: 0044):
~~~
onUnbind.2752.0044=/usr/local/bin/virtualhere/scripts/minidsp_onunbind.sh "$DEVPATH$" "$ADDRESS$" "$VENDOR_ID$" "$PRODUCT_ID$"
~~~
The extra parameters ($DEVPATH$, ...) are entirely optional.

## Problems<br/><small>(and some solutions)</small>
### Problem: the Arduino resets when you open the serial connection in Python

The Arduinos (except the Leonardo) have an auto-reset feature. Disable it with a 10uF capacitor between the RESET pin and GND. For ease of use put a DIP switch between the capacitor and the RESET pin, otherwise you won't be able to upload code to the Arduino.

### Problem: serialEvent() on the Arduino doesn't trigger when receiving data over serial
Add to the end of the Arduino sketch (after the main loop):
~~~C
void serialEventRun(void){ if(Serial.available()) serialEvent() }
~~~

### Problem: The MiniDSP would sometimes hang or return an empty response
This issue mainly happened when changing the volume. The device seemed to hang and would not respond to any commands, when the program was restarted it worked OK, for a time.
1. limit the rate you send the commands over serial (applies mainly to volume up/down). Use the elapsedMillis library for the Arduino and send the command only if ~500 ms have elapsed since the last command. The MiniDSP plugin seems to send volume updates to the device only every 0.5 seconds or so.
2. (In Python) open the connection to the device only when you need to send a command and then close it. Added to transport_usbhid.py/TransportUSBHID:
~~~Python
def close(self):
  try:
    self._hid_device.close()
  except:
    raise RunTimeError("Error closing device.")
~~~
3. (In Python) check if the response from the device is empty. If it is - send the command again. Added to board_2x4hd.py/_masterStatus(self):
~~~Python
while not resp:
  # send command again
  resp = self._transport.write([0x05, 0xff, 0xda, 0x02])
~~~
4. (In Python) set the connection to the device to be non blocking. Added to transport_usbhid.py/\_\_init\_\_(self, vid, pid):
~~~Python
self._hid_device.set_nonblocking(1)
~~~

### Problem: crosstalk on analog inputs and/or outputs when routed over the CD4052 mux/demux chip
Solution(?): Replace the CD4052 with discrete relays or switches.

### Problem: there is noise when the OLED is turned on
Solution: a 10uF electrolytic capacitor across the OLED VCC and GND pins worked for me. Also make sure that the Arduino ground is isolated from the audio ground everywhere, including the end point(s) of the serial connection(s).

### Problem: the Raspberry Pi 4 receives power from the Arduino over USB, preventing power up after a shutdown
It's not enough to power it, but it prevents it from turning on for a minute, or so, after shutdown. Didn't happen on the Pi 3B+.

Solution: Cut the +5V line on the USB cable going from the Arduino to the Pi. Backpowering through USB is, apparently, a <a href="http://www.whatimade.today/how-to-protect-a-rasperry-pi-from-a-powered-usb-hub/">known problem</a>.

## Ideas:

<a href="./ideas-todo.md">Moved to a separate file.</a>

### Old concept renders
![version 1 front](./img/renders/old/v1_front.jpg "version 1 front")
![version 1 back](./img/renders/old/v1_back.jpg "version 1 back")
![version 4 front](./img/renders/old/v4_03.jpg "version 4 front")
![version 4 back](./img/renders/old/v4_back_02.jpg "version 4 back")
![version 4 back detail](./img/renders/old/v4_back_detail.jpg "v4 back detail")
![version 4 front detail](./img/renders/old/v4_corner_detail.jpg "version 4 front detail")
![version 5 front](./img/renders/old/v5_front.jpg "version 5 front")
![version 5 back](./img/renders/old/v5_back.jpg "version 5 back") 