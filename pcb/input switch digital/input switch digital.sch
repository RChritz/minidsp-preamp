EESchema Schematic File Version 4
LIBS:input switch digital-cache
EELAYER 26 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L cliff_toslink:Cliff_ORJ-5 TORX1
U 1 1 5CFD1367
P 1800 1800
F 0 "TORX1" H 2327 1853 60  0000 L CNN
F 1 "Cliff_ORJ-5" H 2327 1747 60  0000 L CNN
F 2 "Cliff_Toslink:Cliff_ORJ-5" H 1800 1800 60  0001 C CNN
F 3 "" H 1800 1800 60  0001 C CNN
	1    1800 1800
	0    -1   -1   0   
$EndComp
$Comp
L 4xxx:4052 U1
U 1 1 5CFD151A
P 3700 4700
F 0 "U1" V 3654 5641 50  0000 L CNN
F 1 "4052" V 3745 5641 50  0000 L CNN
F 2 "Housings_DIP:DIP-16_W7.62mm_Socket_LongPads" H 3700 4700 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4051bms-52bms-53bms.pdf" H 3700 4700 50  0001 C CNN
	1    3700 4700
	0    1    1    0   
$EndComp
$Comp
L cliff_toslink:Cliff_ORJ-5 TORX2
U 1 1 5CFD15B3
P 3400 1800
F 0 "TORX2" H 3927 1853 60  0000 L CNN
F 1 "Cliff_ORJ-5" H 3927 1747 60  0000 L CNN
F 2 "Cliff_Toslink:Cliff_ORJ-5" H 3400 1800 60  0001 C CNN
F 3 "" H 3400 1800 60  0001 C CNN
	1    3400 1800
	0    -1   -1   0   
$EndComp
$Comp
L cliff_toslink:Cliff_OTJ-5 TOTX1
U 1 1 5CFD1670
P 3250 6100
F 0 "TOTX1" V 3060 5872 60  0000 R CNN
F 1 "Cliff_OTJ-5" V 2954 5872 60  0000 R CNN
F 2 "Cliff_Toslink:Cliff_OTJ-5" H 3250 6100 60  0001 C CNN
F 3 "" H 3250 6100 60  0001 C CNN
	1    3250 6100
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x04_Male J1
U 1 1 5CFD1702
P 6100 4750
F 0 "J1" V 6253 4463 50  0000 R CNN
F 1 "Conn_01x04_Male" V 6162 4463 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 6100 4750 50  0001 C CNN
F 3 "~" H 6100 4750 50  0001 C CNN
	1    6100 4750
	0    -1   -1   0   
$EndComp
Text GLabel 6000 4550 1    50   Input ~ 0
VCC
Text GLabel 6100 4550 1    50   Input ~ 0
GND
Text GLabel 6200 4550 1    50   Input ~ 0
A-DIGI
$Comp
L Device:C C5
U 1 1 5CFD3D15
P 1650 2450
F 0 "C5" V 1398 2450 50  0000 C CNN
F 1 "15pF" V 1489 2450 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1688 2300 50  0001 C CNN
F 3 "~" H 1650 2450 50  0001 C CNN
	1    1650 2450
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 5CFD3DC0
P 1950 2200
F 0 "C1" V 1698 2200 50  0000 C CNN
F 1 "0.1uF" V 1789 2200 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1988 2050 50  0001 C CNN
F 3 "~" H 1950 2200 50  0001 C CNN
	1    1950 2200
	0    1    1    0   
$EndComp
$Comp
L Device:C C9
U 1 1 5CFD3DEF
P 1650 2850
F 0 "C9" V 1398 2850 50  0000 C CNN
F 1 "15pF" V 1489 2850 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1688 2700 50  0001 C CNN
F 3 "~" H 1650 2850 50  0001 C CNN
	1    1650 2850
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 5CFD3E1C
P 3550 2200
F 0 "C2" V 3298 2200 50  0000 C CNN
F 1 "0.1uF" V 3389 2200 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3588 2050 50  0001 C CNN
F 3 "~" H 3550 2200 50  0001 C CNN
	1    3550 2200
	0    1    1    0   
$EndComp
$Comp
L Device:C C6
U 1 1 5CFD3E58
P 3250 2450
F 0 "C6" V 2998 2450 50  0000 C CNN
F 1 "15pF" V 3089 2450 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3288 2300 50  0001 C CNN
F 3 "~" H 3250 2450 50  0001 C CNN
	1    3250 2450
	0    1    1    0   
$EndComp
$Comp
L Device:C C10
U 1 1 5CFD3E8C
P 3250 2900
F 0 "C10" V 2998 2900 50  0000 C CNN
F 1 "15pF" V 3089 2900 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3288 2750 50  0001 C CNN
F 3 "~" H 3250 2900 50  0001 C CNN
	1    3250 2900
	0    1    1    0   
$EndComp
$Comp
L Device:C C13
U 1 1 5CFD3EBF
P 3400 5800
F 0 "C13" V 3148 5800 50  0000 C CNN
F 1 "0.1uF" V 3239 5800 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3438 5650 50  0001 C CNN
F 3 "~" H 3400 5800 50  0001 C CNN
	1    3400 5800
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 1800 1800 2200
Wire Wire Line
	1800 2200 1800 2450
Connection ~ 1800 2200
Wire Wire Line
	1800 2450 1800 2850
Connection ~ 1800 2450
Wire Wire Line
	1700 1800 1700 2200
Wire Wire Line
	1700 2200 1500 2200
Wire Wire Line
	1500 2200 1500 2450
Connection ~ 1500 2450
Wire Wire Line
	1500 2450 1500 2850
Wire Wire Line
	1900 1800 1900 2000
Wire Wire Line
	1900 2000 2100 2000
Wire Wire Line
	2100 2000 2100 2200
Wire Wire Line
	1500 2850 1500 3100
Connection ~ 1500 2850
Wire Wire Line
	2100 2200 2100 3100
Connection ~ 2100 2200
Wire Wire Line
	3400 2200 3400 2450
Wire Wire Line
	3400 2450 3400 2900
Connection ~ 3400 2450
Wire Wire Line
	3400 2900 3400 3100
Connection ~ 3400 2900
Wire Wire Line
	3500 2000 3700 2000
Wire Wire Line
	3700 2000 3700 2200
Connection ~ 3700 2200
Wire Wire Line
	3700 2200 3700 3100
Wire Wire Line
	3300 2200 3100 2200
Wire Wire Line
	3100 2200 3100 2450
Connection ~ 3100 2450
Wire Wire Line
	3100 2450 3100 2900
Connection ~ 3100 2900
Wire Wire Line
	3100 2900 3100 3100
Wire Wire Line
	3250 6100 3250 5800
Wire Wire Line
	3350 6100 3350 6000
Wire Wire Line
	3350 6000 3550 6000
Wire Wire Line
	3550 6000 3550 5800
Wire Wire Line
	3150 6100 3150 5600
Wire Wire Line
	3250 5800 3250 5600
Connection ~ 3250 5800
Wire Wire Line
	3550 5800 3550 5600
Connection ~ 3550 5800
Text GLabel 2800 4700 0    50   Input ~ 0
GND
Text GLabel 2800 4800 0    50   Input ~ 0
GND
Text GLabel 3400 4200 1    50   Input ~ 0
GND
Text GLabel 4600 4700 2    50   Input ~ 0
VCC
Text GLabel 3200 4200 1    50   Input ~ 0
A-DIGI
Text GLabel 3250 5600 1    50   Input ~ 0
VCC
Wire Wire Line
	1800 2850 1800 3100
Connection ~ 1800 2850
Text GLabel 2100 3100 3    50   Input ~ 0
VCC
Text GLabel 3700 3100 3    50   Input ~ 0
VCC
Text GLabel 1800 3100 3    50   Input ~ 0
GND
Text GLabel 3400 3100 3    50   Input ~ 0
GND
Text GLabel 3550 5600 1    50   Input ~ 0
GND
Text GLabel 1500 3100 3    50   Input ~ 0
TOS-RX-1
Text GLabel 4300 4200 1    50   Input ~ 0
TOS-RX-1
Text GLabel 3100 3100 3    50   Input ~ 0
TOS-RX-2
Text GLabel 4200 4200 1    50   Input ~ 0
TOS-RX-2
Text GLabel 3150 5600 1    50   Input ~ 0
TOS-TX
Text GLabel 4300 5200 3    50   Input ~ 0
TOS-TX
$Comp
L cliff_toslink:Cliff_ORJ-5 TORX3
U 1 1 5CFD361D
P 4850 1800
F 0 "TORX3" H 5377 1853 60  0000 L CNN
F 1 "Cliff_ORJ-5" H 5377 1747 60  0000 L CNN
F 2 "Cliff_Toslink:Cliff_ORJ-5" H 4850 1800 60  0001 C CNN
F 3 "" H 4850 1800 60  0001 C CNN
	1    4850 1800
	0    -1   -1   0   
$EndComp
$Comp
L cliff_toslink:Cliff_ORJ-5 TORX4
U 1 1 5CFD36B0
P 6150 1800
F 0 "TORX4" H 6677 1853 60  0000 L CNN
F 1 "Cliff_ORJ-5" H 6677 1747 60  0000 L CNN
F 2 "Cliff_Toslink:Cliff_ORJ-5" H 6150 1800 60  0001 C CNN
F 3 "" H 6150 1800 60  0001 C CNN
	1    6150 1800
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C3
U 1 1 5CFD3714
P 5000 2200
F 0 "C3" V 4748 2200 50  0000 C CNN
F 1 "0.1uF" V 4839 2200 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5038 2050 50  0001 C CNN
F 3 "~" H 5000 2200 50  0001 C CNN
	1    5000 2200
	0    1    1    0   
$EndComp
$Comp
L Device:C C7
U 1 1 5CFD37E0
P 4700 2450
F 0 "C7" V 4448 2450 50  0000 C CNN
F 1 "15pF" V 4539 2450 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4738 2300 50  0001 C CNN
F 3 "~" H 4700 2450 50  0001 C CNN
	1    4700 2450
	0    1    1    0   
$EndComp
$Comp
L Device:C C11
U 1 1 5CFD3839
P 4700 2850
F 0 "C11" V 4448 2850 50  0000 C CNN
F 1 "15pF" V 4539 2850 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4738 2700 50  0001 C CNN
F 3 "~" H 4700 2850 50  0001 C CNN
	1    4700 2850
	0    1    1    0   
$EndComp
$Comp
L Device:C C4
U 1 1 5CFD388C
P 6300 2200
F 0 "C4" V 6048 2200 50  0000 C CNN
F 1 "0.1uF" V 6139 2200 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6338 2050 50  0001 C CNN
F 3 "~" H 6300 2200 50  0001 C CNN
	1    6300 2200
	0    1    1    0   
$EndComp
$Comp
L Device:C C8
U 1 1 5CFD38DF
P 6000 2450
F 0 "C8" V 5748 2450 50  0000 C CNN
F 1 "15pF" V 5839 2450 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6038 2300 50  0001 C CNN
F 3 "~" H 6000 2450 50  0001 C CNN
	1    6000 2450
	0    1    1    0   
$EndComp
$Comp
L Device:C C12
U 1 1 5CFD3935
P 6000 2900
F 0 "C12" V 5748 2900 50  0000 C CNN
F 1 "15pF" V 5839 2900 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6038 2750 50  0001 C CNN
F 3 "~" H 6000 2900 50  0001 C CNN
	1    6000 2900
	0    1    1    0   
$EndComp
Wire Wire Line
	4850 1800 4850 2200
Connection ~ 4850 2200
Wire Wire Line
	4850 2200 4850 2450
Connection ~ 4850 2450
Wire Wire Line
	4850 2450 4850 2850
Connection ~ 4850 2850
Wire Wire Line
	4850 2850 4850 3100
Wire Wire Line
	4950 1800 4950 2000
Wire Wire Line
	4950 2000 5150 2000
Wire Wire Line
	5150 2000 5150 2200
Connection ~ 5150 2200
Wire Wire Line
	5150 2200 5150 3100
Wire Wire Line
	6150 1800 6150 2200
Connection ~ 6150 2200
Wire Wire Line
	6150 2200 6150 2450
Connection ~ 6150 2450
Wire Wire Line
	6150 2450 6150 2900
Connection ~ 6150 2900
Wire Wire Line
	6150 2900 6150 3100
Wire Wire Line
	6250 2000 6450 2000
Wire Wire Line
	6450 2000 6450 2200
Connection ~ 6450 2200
Wire Wire Line
	6450 2200 6450 3100
Wire Wire Line
	6050 1800 6050 2200
Wire Wire Line
	6050 2200 5850 2200
Wire Wire Line
	5850 2200 5850 2450
Connection ~ 5850 2450
Wire Wire Line
	5850 2450 5850 2900
Connection ~ 5850 2900
Wire Wire Line
	5850 2900 5850 3100
Wire Wire Line
	4750 1800 4750 2200
Wire Wire Line
	4750 2200 4550 2200
Wire Wire Line
	4550 2200 4550 2450
Connection ~ 4550 2450
Wire Wire Line
	4550 2450 4550 2850
Connection ~ 4550 2850
Wire Wire Line
	4550 2850 4550 3100
Text GLabel 5150 3100 3    50   Input ~ 0
VCC
Text GLabel 6450 3100 3    50   Input ~ 0
VCC
Text GLabel 6150 3100 3    50   Input ~ 0
GND
Text GLabel 4850 3100 3    50   Input ~ 0
GND
Text GLabel 4550 3100 3    50   Input ~ 0
TOS-RX-3
Text GLabel 4100 4200 1    50   Input ~ 0
TOS-RX-3
Text GLabel 5850 3100 3    50   Input ~ 0
TOS-RX-4
Text GLabel 4000 4200 1    50   Input ~ 0
TOS-RX-4
Text GLabel 6300 4550 1    50   Input ~ 0
B-DIGI
Text GLabel 3100 4200 1    50   Input ~ 0
B-DIGI
Wire Wire Line
	6250 1800 6250 2000
Wire Wire Line
	3500 2000 3500 1800
Wire Wire Line
	3400 2200 3400 1800
Connection ~ 3400 2200
Wire Wire Line
	3300 2200 3300 1800
$EndSCHEMATC
