G04 #@! TF.GenerationSoftware,KiCad,Pcbnew,(5.0.0-3-g5ebb6b6)*
G04 #@! TF.CreationDate,2019-10-14T08:47:04+02:00*
G04 #@! TF.ProjectId,front panel,66726F6E742070616E656C2E6B696361,rev?*
G04 #@! TF.SameCoordinates,Original*
G04 #@! TF.FileFunction,Soldermask,Top*
G04 #@! TF.FilePolarity,Negative*
%FSLAX46Y46*%
G04 Gerber Fmt 4.6, Leading zero omitted, Abs format (unit mm)*
G04 Created by KiCad (PCBNEW (5.0.0-3-g5ebb6b6)) date 2019 October 14, Monday 08:47:04*
%MOMM*%
%LPD*%
G01*
G04 APERTURE LIST*
%ADD10C,3.600000*%
%ADD11C,2.400000*%
%ADD12R,2.100000X2.100000*%
%ADD13O,2.100000X2.100000*%
G04 APERTURE END LIST*
D10*
G04 #@! TO.C,REF\002A\002A*
X173250000Y-103750000D03*
G04 #@! TD*
D11*
G04 #@! TO.C,SW1*
X100000000Y-106000000D03*
X100000000Y-101500000D03*
X106500000Y-106000000D03*
X106500000Y-101500000D03*
G04 #@! TD*
D10*
G04 #@! TO.C,REF\002A\002A*
X93250000Y-103750000D03*
G04 #@! TD*
G04 #@! TO.C,REF\002A\002A*
X133250000Y-103750000D03*
G04 #@! TD*
D12*
G04 #@! TO.C,J2*
X120000000Y-110500000D03*
D13*
X117460000Y-110500000D03*
G04 #@! TD*
G04 #@! TO.C,J3*
X137460000Y-110500000D03*
D12*
X140000000Y-110500000D03*
G04 #@! TD*
D11*
G04 #@! TO.C,SW2*
X126500000Y-101500000D03*
X126500000Y-106000000D03*
X120000000Y-101500000D03*
X120000000Y-106000000D03*
G04 #@! TD*
D12*
G04 #@! TO.C,J1*
X100000000Y-110500000D03*
D13*
X97460000Y-110500000D03*
G04 #@! TD*
G04 #@! TO.C,J4*
X157460000Y-110500000D03*
D12*
X160000000Y-110500000D03*
G04 #@! TD*
D11*
G04 #@! TO.C,SW3*
X140000000Y-106000000D03*
X140000000Y-101500000D03*
X146500000Y-106000000D03*
X146500000Y-101500000D03*
G04 #@! TD*
G04 #@! TO.C,SW4*
X166500000Y-101500000D03*
X166500000Y-106000000D03*
X160000000Y-101500000D03*
X160000000Y-106000000D03*
G04 #@! TD*
M02*
