EESchema Schematic File Version 4
LIBS:front panel-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 5D0046F3
P 2500 2200
F 0 "J1" V 2560 2241 50  0000 L CNN
F 1 "BTN_1" V 2651 2241 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2500 2200 50  0001 C CNN
F 3 "~" H 2500 2200 50  0001 C CNN
	1    2500 2200
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Male J2
U 1 1 5D00473A
P 3100 2200
F 0 "J2" V 3160 2241 50  0000 L CNN
F 1 "BTN_2" V 3251 2241 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3100 2200 50  0001 C CNN
F 3 "~" H 3100 2200 50  0001 C CNN
	1    3100 2200
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 5D0048DF
P 2450 3200
F 0 "SW1" H 2450 3485 50  0000 C CNN
F 1 "BTN_1" H 2450 3394 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 2450 3400 50  0001 C CNN
F 3 "" H 2450 3400 50  0001 C CNN
	1    2450 3200
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 5D004938
P 3050 3200
F 0 "SW2" H 3050 3485 50  0000 C CNN
F 1 "BTN_2" H 3050 3394 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 3050 3400 50  0001 C CNN
F 3 "" H 3050 3400 50  0001 C CNN
	1    3050 3200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J3
U 1 1 5D005159
P 3700 2200
F 0 "J3" V 3760 2241 50  0000 L CNN
F 1 "BTN_3" V 3851 2241 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3700 2200 50  0001 C CNN
F 3 "~" H 3700 2200 50  0001 C CNN
	1    3700 2200
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Male J4
U 1 1 5D0051AB
P 4300 2200
F 0 "J4" V 4360 2241 50  0000 L CNN
F 1 "BTN_4" V 4451 2241 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4300 2200 50  0001 C CNN
F 3 "~" H 4300 2200 50  0001 C CNN
	1    4300 2200
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 5D005282
P 3650 3200
F 0 "SW3" H 3650 3485 50  0000 C CNN
F 1 "BTN_3" H 3650 3394 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 3650 3400 50  0001 C CNN
F 3 "" H 3650 3400 50  0001 C CNN
	1    3650 3200
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 5D0052C3
P 4250 3200
F 0 "SW4" H 4250 3485 50  0000 C CNN
F 1 "BTN_4" H 4250 3394 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 4250 3400 50  0001 C CNN
F 3 "" H 4250 3400 50  0001 C CNN
	1    4250 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 3200 3450 2700
Wire Wire Line
	3450 2700 3600 2700
Wire Wire Line
	3600 2700 3600 2400
Wire Wire Line
	4050 3200 4050 2700
Wire Wire Line
	4050 2700 4200 2700
Wire Wire Line
	4200 2700 4200 2400
Wire Wire Line
	2850 3200 2850 2700
Wire Wire Line
	2850 2700 3000 2700
Wire Wire Line
	3000 2700 3000 2400
Wire Wire Line
	2250 3200 2250 2700
Wire Wire Line
	2250 2700 2400 2700
Wire Wire Line
	2400 2700 2400 2400
Wire Wire Line
	2500 2400 2500 2700
Wire Wire Line
	2500 2700 2650 2700
Wire Wire Line
	2650 2700 2650 3200
Wire Wire Line
	2650 3200 2650 3300
Wire Wire Line
	2650 3300 3250 3300
Wire Wire Line
	3250 3300 3250 3200
Connection ~ 2650 3200
Wire Wire Line
	3250 3300 3850 3300
Wire Wire Line
	3850 3300 3850 3200
Connection ~ 3250 3300
Wire Wire Line
	3850 3300 4450 3300
Wire Wire Line
	4450 3300 4450 3200
Connection ~ 3850 3300
$EndSCHEMATC
